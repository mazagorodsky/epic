package com.epam.hibernate.naming;

import org.hibernate.boot.model.naming.Identifier;
import org.hibernate.boot.model.naming.ImplicitForeignKeyNameSource;
import org.hibernate.boot.model.naming.ImplicitNamingStrategyJpaCompliantImpl;

/**
 * @author alex on 17/3/17.
 *         stackoverflow.com/documentation/hibernate/3051/custom-naming-strategy/10369/creating-and-using-a-custom-implicitnamingstrategy
 */
public class CustomImplicitNamingStrategy extends ImplicitNamingStrategyJpaCompliantImpl {

    public static final CustomImplicitNamingStrategy INSTANCE = new CustomImplicitNamingStrategy();

    @Override
    public Identifier determineForeignKeyName(ImplicitForeignKeyNameSource source) {

        String name =
                "FK_" + source.getTableName().getCanonicalName() +
                        "_XX_" + source.getReferencedTableName().getCanonicalName();

        String upperCased = name.toUpperCase();

        return toIdentifier(upperCased, source.getBuildingContext());
    }
}
