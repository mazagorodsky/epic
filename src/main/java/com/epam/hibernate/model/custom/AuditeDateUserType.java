package com.epam.hibernate.model.custom;

import org.hibernate.HibernateException;
import org.hibernate.engine.spi.SharedSessionContractImplementor;
import org.hibernate.type.StandardBasicTypes;
import org.hibernate.usertype.UserType;

import java.io.Serializable;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Date;

/**
 * A custom Hibernate type that maps between com.epam.hibernate.model.custom.AuditDate and 2 values of
 * java.sql.Timestamp (createdDate and modifiedDate).
 * <p>
 * source: internetka.in.ua/hibernate-user-type-part1
 *
 * @author alex on 19/3/17.
 */
public class AuditeDateUserType implements UserType {
    @Override
    public int[] sqlTypes() {
        return new int[]{StandardBasicTypes.TIMESTAMP.sqlType(), StandardBasicTypes.TIMESTAMP.sqlType()};
    }

    @Override
    public Class returnedClass() {
        return AuditDate.class;
    }

    @Override
    public boolean equals(Object x, Object y) throws HibernateException {
        return !(x == null) && x.equals(y);
    }

    @Override
    public int hashCode(Object x) throws HibernateException {
        return x.hashCode();
    }

    @Override
    public Object nullSafeGet(ResultSet rs, String[] names, SharedSessionContractImplementor session, Object owner) throws HibernateException, SQLException {
        AuditDate auditDate = new AuditDate();

        Timestamp created = rs.getTimestamp(names[0]);
        auditDate.setCreatedDate(created);

        Timestamp modified = rs.getTimestamp(names[1]);
        auditDate.setModifiedDate(modified);

        return auditDate;
    }

    @Override
    public void nullSafeSet(PreparedStatement st, Object value, int index, SharedSessionContractImplementor session) throws HibernateException, SQLException {
        AuditDate auditDate = (AuditDate) value;

        Date created = auditDate.getCreatedDate();
        if (created != null) {
            st.setTimestamp(index, new Timestamp(created.getTime()));
        }

        Date modified = auditDate.getModifiedDate();
        if (modified != null) {
            st.setTimestamp(index + 1, new Timestamp(modified.getTime()));
        }
    }

    @Override
    public Object deepCopy(Object value) throws HibernateException {
        return new AuditDate(((AuditDate) value).getCreatedDate(), ((AuditDate) value).getModifiedDate());
    }

    @Override
    public boolean isMutable() {
        return true;
    }

    @Override
    public Serializable disassemble(Object value) throws HibernateException {
        return (Serializable) deepCopy(value);
    }

    @Override
    public Object assemble(Serializable cached, Object owner) throws HibernateException {
        return deepCopy(cached);
    }

    @Override
    public Object replace(Object original, Object target, Object owner) throws HibernateException {
        return deepCopy(original);
    }
}
