package com.epam.hibernate.model.custom;

import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

/**
 * @author alex on 19/3/17.
 */
public class AuditDate implements Serializable {

    private Date createdDate;
    private Date modifiedDate;

    public AuditDate(Date createdDate, Date modifiedDate) {
        this.createdDate = createdDate;
        this.modifiedDate = modifiedDate;
    }

    protected AuditDate() {
        //for Hibernate
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Date getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(Date modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AuditDate auditDate = (AuditDate) o;
        return Objects.equals(createdDate, auditDate.createdDate) &&
                Objects.equals(modifiedDate, auditDate.modifiedDate);
    }

    @Override
    public int hashCode() {
        return Objects.hash(createdDate, modifiedDate);
    }

    @Override
    public String toString() {
        return "AuditDate{" +
                "createdDate=" + createdDate +
                ", modifiedDate=" + modifiedDate +
                '}';
    }
}
