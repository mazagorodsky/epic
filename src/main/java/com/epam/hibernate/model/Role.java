package com.epam.hibernate.model;

import javax.persistence.*;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author alex on 25/2/17.
 */
@Entity
// @Table(name = "ROLE")
@NamedEntityGraph(name = "role.users", attributeNodes = {@NamedAttributeNode("users")/*, @NamedAttributeNode("accessList")*/},
        subgraphs = @NamedSubgraph(name = "users.creds", attributeNodes = @NamedAttributeNode("credentials")))
public class Role {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY) //uses special IDENTITY column, increments on insertion [slow]
    // @Column(name = "ID")
    private int id;

    @Enumerated(EnumType.STRING) //used to store the Enum as the STRING value of its name
    @Column(/*name = "NAME",*/ unique = true, nullable = false, length = 50)
    private Title name;

    @OneToMany(mappedBy = "role")
    private List<User> users;

    @OneToMany(mappedBy = "role")
    @MapKey(name = "name")
    private Map<String, User> usersByName; //example of a persistent Map

    @ManyToMany
    @JoinTable(name = "ROLE_X_ACCESS",
            joinColumns = @JoinColumn(name = "ROLE_ID", referencedColumnName = "ID"),
            inverseJoinColumns = @JoinColumn(name = "ACCESS_ID", referencedColumnName = "ID"))
    private List<Access> accessList;

    public Role(Title name) {
        this.name = name;
    }

    protected Role() {
        // for Hibernate
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Title getName() {
        return name;
    }

    public void setName(Title name) {
        this.name = name;
    }

    public List<User> getUsers() {
        return users;
    }

    public void setUsers(List<User> users) {
        this.users = users;
    }

    public Map<String, User> getUsersByName() {
        return usersByName;
    }

    public void setUsersByName(Map<String, User> usersByName) {
        this.usersByName = usersByName;
    }

    public List<Access> getAccessList() {
        return accessList;
    }

    public void setAccessList(List<Access> accessList) {
        this.accessList = accessList;
    }

    @Override
    public String toString() {
        return "Role{" +
                "id=" + id +
                ", name=" + name +
                ", accessList=" + accessList.stream().map(Access::getAccessLevel).collect(Collectors.toList()) +
                ", users=" + users.stream().map(User::getName).collect(Collectors.toList()) +
                '}';
    }

    public enum Title {
        USER, MANAGER, ADMIN
    }
}
