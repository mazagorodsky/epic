package com.epam.hibernate.model;

import javax.persistence.*;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author alex on 4/3/17.
 */
@Entity
// @Table(name = "ACCESS")
public class Access {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY) //uses special IDENTITY column, increments on insertion [slow]
    // @Column(name = "ID")
    private int id;

    @Basic //default, may be omitted
    @Enumerated(EnumType.STRING) //used to store the Enum as the STRING value of its name
    // @Column(name = "ACCESS_LEVEL")
    private Privilege accessLevel;

    @ManyToMany(mappedBy = "accessList")
    private List<Role> roles;

    protected Access() {
        // for Hibernate
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Privilege getAccessLevel() {
        return accessLevel;
    }

    public void setAccessLevel(Privilege accessLevel) {
        this.accessLevel = accessLevel;
    }

    @Override
    public String toString() {
        return "Access{" +
                "id=" + id +
                ", accessLevel=" + accessLevel +
                ", roles=" + roles.stream().map(Role::getName).collect(Collectors.toList()) +
                '}';
    }

    public enum Privilege {
        CREATE, READ, UPDATE, DELETE
    }
}
