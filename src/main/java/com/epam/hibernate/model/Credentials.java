package com.epam.hibernate.model;

import javax.persistence.*;

/**
 * @author alex on 5/3/17.
 */
@Entity
// @Table(name = "CREDENTIALS")
public class Credentials {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    // @Column(name = "ID")
    private int id;

    // @Column(name = "NICKNAME")
    private String nickname;

    // @Column(name = "PASSWORD")
    private String password;

    @OneToOne(mappedBy = "credentials", fetch = FetchType.LAZY) // inverse OneToOne relationship
    private User user; //credentials owner

    public Credentials(String nickname, String password) {
        this.nickname = nickname;
        this.password = password;
    }

    protected Credentials() {
        // for Hibernate
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @Override
    public String toString() {
        return "Credentials{" +
                "id=" + id +
                ", nickname='" + nickname + '\'' +
                ", user=" + user.getName() +
                '}';
    }
}
