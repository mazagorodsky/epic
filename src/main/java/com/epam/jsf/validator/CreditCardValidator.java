package com.epam.jsf.validator;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;
import java.io.Serializable;

/**
 * Created by Maksim on 2/17/2017.
 */
@FacesValidator("CreditCardValidator")
public class CreditCardValidator implements Validator, Serializable {
    private String errorSummary;
    private String errorDetail;

    public void validate(FacesContext facesContext, UIComponent uiComponent, Object o) throws ValidatorException {
        System.out.println("Validation...");
    }
}
